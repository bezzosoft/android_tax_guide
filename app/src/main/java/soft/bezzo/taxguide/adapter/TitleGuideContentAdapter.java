package soft.bezzo.taxguide.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import soft.bezzo.taxguide.R;
import soft.bezzo.taxguide.model.GuideContent;

/**
 * Created by bezzo on 11/03/17.
 */

public class TitleGuideContentAdapter extends RecyclerView.Adapter<TitleGuideContentAdapter.ViewHolder>{

    private ArrayList<GuideContent> mGuideContent;
    private Context mContext;

    //define listener member variable
    private static OnItemClickListener listener;
    //define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }
    //define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.ivTitleGuideContent)
            AppCompatImageView titleGuideContentIV;
        @BindView(R.id.tvTitleGuideContent)
            AppCompatTextView titleGuideContentTV;
        @BindView(R.id.ibTitleGuideContent)
            AppCompatImageButton titleGuideContentIB;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            //setup the click listener
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Triggers click upwards to the adapter on click
                    if (listener != null){
                        listener.onItemClick(view, getLayoutPosition());
                    }
                }
            });
        }
    }

    public TitleGuideContentAdapter(Context context, ArrayList<GuideContent> guideContents){
        this.mGuideContent = guideContents;
        this.mContext = context;
    }

    @Override
    public TitleGuideContentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        //inflate custom layout
        View guideContentView = inflater.inflate(R.layout.item_guide_content, parent, false);

        //return a new holder instance
        ViewHolder viewHolder = new ViewHolder(guideContentView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TitleGuideContentAdapter.ViewHolder holder, int position) {
        //get data base on position
        GuideContent guideContent = mGuideContent.get(position);

        holder.titleGuideContentTV.setText(guideContent.getTitleGuideContent());
    }

    @Override
    public int getItemCount() {
        return mGuideContent.size();
    }

    public void update(ArrayList<GuideContent> guideContents){
        this.mGuideContent.clear();

        this.mGuideContent.addAll(guideContents);

        notifyDataSetChanged();
    }

    public void clear(){
        mGuideContent.clear();
        notifyDataSetChanged();
    }
}
