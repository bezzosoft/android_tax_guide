package soft.bezzo.taxguide.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import soft.bezzo.taxguide.R;
import soft.bezzo.taxguide.model.ContentGuide;

/**
 * Created by bezzo on 14/03/17.
 */

public class ContentGuideAdapter extends RecyclerView.Adapter<ContentGuideAdapter.ViewHolder>{

    private ArrayList<ContentGuide> mContentGuides;
    private Context context;

    public interface OnItemClickListener{
        void onItemClick(View itemView, int position);
    }

    private static OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }

    @Override
    public ContentGuideAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View contentGuideView = layoutInflater.inflate(R.layout.item_content_guide, parent,
                false);

        ViewHolder viewHolder = new ViewHolder(contentGuideView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ContentGuide contentGuide = mContentGuides.get(position);

        holder.contentGuideTV.setText(contentGuide.getTitleContent());
        holder.subTitleContentGuideTV.setText(contentGuide.getSubGuideContent());
    }

    @Override
    public int getItemCount() {
        return mContentGuides.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tvContentGuide)
            AppCompatTextView contentGuideTV;
        @BindView(R.id.tvSubTitleContentGuide)
            AppCompatTextView subTitleContentGuideTV;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null){
                        listener.onItemClick(view, getLayoutPosition());
                    }
                }
            });
        }
    }

    public ContentGuideAdapter(Context context, ArrayList<ContentGuide> contentGuides){
        this.context = context;
        this.mContentGuides = contentGuides;
    }

    public void clear(){
        mContentGuides.clear();
        notifyDataSetChanged();
    }

    public void update(ArrayList<ContentGuide> contentGuides){
        this.mContentGuides.clear();
        this.mContentGuides.addAll(contentGuides);
        notifyDataSetChanged();
    }
}
