package soft.bezzo.taxguide.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import soft.bezzo.taxguide.R;
import soft.bezzo.taxguide.model.SubGuideContent;

/**
 * Created by bezzo on 13/03/17.
 */


// Create the basic adapter extending from RecyclerView.Adapter
// Note that we specify the custom ViewHolder which gives us access to our views
public class SubGuideContentAdapter extends RecyclerView.Adapter<SubGuideContentAdapter.ViewHolder> {

    private ArrayList<SubGuideContent> mSubGuideContents;
    private Context context;

    public interface OnItemClickListener{
        void onItemClick(View itemView, int position);
    }

    private static OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }

    @Override
    public SubGuideContentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View subGuideContentView = layoutInflater.inflate(R.layout.sub_item_guide_content, parent,
                false);

        ViewHolder viewHolder = new ViewHolder(subGuideContentView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SubGuideContentAdapter.ViewHolder holder, int position) {
        SubGuideContent subGuideContent = mSubGuideContents.get(position);

        holder.tvSubGuideContent.setText(subGuideContent.getTitleGuideContent());
        holder.tvSubTitleGuideContent.setText(subGuideContent.getSubGuideContent());
    }

    @Override
    public int getItemCount() {
        return mSubGuideContents.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvSubGuideContent) AppCompatTextView tvSubGuideContent;
        @BindView(R.id.tvSubTitleGuideContent) AppCompatTextView tvSubTitleGuideContent;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null){
                        listener.onItemClick(view, getLayoutPosition());
                    }
                }
            });
        }
    }

    public SubGuideContentAdapter(Context context, ArrayList<SubGuideContent> mSubGuideContents){
        this.context = context;
        this.mSubGuideContents = mSubGuideContents;
    }

    public void clear(){
        mSubGuideContents.clear();
        notifyDataSetChanged();
    }

    public void update(ArrayList<SubGuideContent> mSubGuideContents){
        this.mSubGuideContents.clear();
        this.mSubGuideContents.addAll(mSubGuideContents);
        notifyDataSetChanged();
    }
}
