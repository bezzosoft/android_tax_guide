package soft.bezzo.taxguide.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by bezzo on 11/03/17.
 */

public class Client {

    public static final String BASE_URL = "http://192.168.100.14:8000/";

    public static String getBaseUrl() {
        return BASE_URL;
    }

    public static Request getApi(){
        //Builder
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Request apiService = retrofit.create(Request.class);
        return apiService;
    }
}
