package soft.bezzo.taxguide.api;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import soft.bezzo.taxguide.model.ContentGuide;
import soft.bezzo.taxguide.model.GuideContent;
import soft.bezzo.taxguide.model.SubGuideContent;

/**
 * Created by bezzo on 11/03/17.
 */

public interface Request {
    @GET("get-guide-contents/title")
    Call<ArrayList<GuideContent>> getTitleGuideContent();

    @GET("get-sub-guide-contents/{id}")
    Call<ArrayList<SubGuideContent>> getSubTitleGuideContent(@Path("id") String id);

    @GET("get-content-subguide/{id}")
    Call<ArrayList<ContentGuide>> getContentGuide(@Path("id") String id);
}
