package soft.bezzo.taxguide.model;

/**
 * Created by bezzo on 14/03/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContentGuide {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title_content")
    @Expose
    private String titleContent;
    @SerializedName("sub_guide_content")
    @Expose
    private String subGuideContent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitleContent() {
        return titleContent;
    }

    public void setTitleContent(String titleContent) {
        this.titleContent = titleContent;
    }

    public String getSubGuideContent() {
        return subGuideContent;
    }

    public void setSubGuideContent(String subGuideContent) {
        this.subGuideContent = subGuideContent;
    }

}