package soft.bezzo.taxguide.model;

/**
 * Created by bezzo on 11/03/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GuideContent {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title_guide_content")
    @Expose
    private String titleGuideContent;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("guide_contents_id")
    @Expose
    private Object guideContentsId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitleGuideContent() {
        return titleGuideContent;
    }

    public void setTitleGuideContent(String titleGuideContent) {
        this.titleGuideContent = titleGuideContent;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getGuideContentsId() {
        return guideContentsId;
    }

    public void setGuideContentsId(Object guideContentsId) {
        this.guideContentsId = guideContentsId;
    }

}