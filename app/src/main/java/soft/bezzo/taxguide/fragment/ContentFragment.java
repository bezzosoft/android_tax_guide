package soft.bezzo.taxguide.fragment;

import android.annotation.TargetApi;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import soft.bezzo.taxguide.R;
import soft.bezzo.taxguide.api.Client;

/**
 * Created by bezzo on 14/03/17.
 */

public class ContentFragment extends Fragment {

    @BindView(R.id.wvContent) WebView contentWV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_content, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        settingBrowserContent();
        Bundle arguments = getArguments();
        contentWV.setWebViewClient(new MyBrowser());
        String path = Uri.parse(Client.getBaseUrl() + "get-content/"
                + arguments.getString("id_content")).toString();
        contentWV.loadUrl(path);
    }

    public void settingBrowserContent(){
        //configure related browser settings
        contentWV.getSettings().setLoadsImagesAutomatically(true);
        contentWV.getSettings().setJavaScriptEnabled(true);
        contentWV.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        //enable responsive layout
        contentWV.getSettings().setUseWideViewPort(true);
        //zoom out if the content width is greater than the width of the viewport
        contentWV.getSettings().setLoadWithOverviewMode(true);
        //configure the client to use when opening URLs
        contentWV.setWebViewClient(new MyBrowser());
        //enable zoom
        contentWV.getSettings().setSupportZoom(true);
        //allow pinch to zoom
        contentWV.getSettings().setBuiltInZoomControls(true);
        //disable the default zoom controls on the page
        contentWV.getSettings().setDisplayZoomControls(false);
    }

    private class MyBrowser extends WebViewClient{

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            view.loadUrl(request.getUrl().toString());
            return true;
        }
    }
}
