package soft.bezzo.taxguide.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import soft.bezzo.taxguide.R;
import soft.bezzo.taxguide.adapter.ContentGuideAdapter;
import soft.bezzo.taxguide.model.ContentGuide;

/**
 * Created by bezzo on 14/03/17.
 */

import static soft.bezzo.taxguide.api.Client.getApi;

public class ContentGuideFragment extends Fragment {

    @BindView(R.id.rvGuideContents)
        RecyclerView guideContentRV;
    private ArrayList<ContentGuide> mContentGuides;
    private ContentGuideAdapter mContentGuideAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        guideContentRV.addItemDecoration(new DividerItemDecoration(getContext(), 1));
        mContentGuides = new ArrayList<ContentGuide>();
        mContentGuideAdapter = new ContentGuideAdapter(getContext(), mContentGuides);
        guideContentRV.setLayoutManager(new LinearLayoutManager(getContext()));
        guideContentRV.setAdapter(mContentGuideAdapter);

        mContentGuideAdapter.setOnItemClickListener(new ContentGuideAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                String data = mContentGuides.get(position).getId().toString();
                changetFragmentToContent(data);
            }
        });
    }

    public void getDataContentGuide(String id){
        Call<ArrayList<ContentGuide>> call = getApi().getContentGuide(id);
        call.enqueue(new Callback<ArrayList<ContentGuide>>() {
            @Override
            public void onResponse(Call<ArrayList<ContentGuide>> call,
                                   Response<ArrayList<ContentGuide>> response) {
                mContentGuideAdapter.update(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<ContentGuide>> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle arguments = getArguments();
        getDataContentGuide(arguments.getString("id_sub_guide"));
    }

    public void changetFragmentToContent(String id){
        Bundle arguments = new Bundle();
        arguments.putString("id_content", id);
        ContentFragment newFragment = new ContentFragment();
        newFragment.setArguments(arguments);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack if needed
        transaction.replace(R.id.flContent, newFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }
}
