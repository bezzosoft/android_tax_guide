package soft.bezzo.taxguide.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import soft.bezzo.taxguide.R;
import soft.bezzo.taxguide.adapter.TitleGuideContentAdapter;
import soft.bezzo.taxguide.model.GuideContent;

/**
 * Created by bezzo on 11/02/17.
 */

import static soft.bezzo.taxguide.api.Client.getApi;

public class HomeFragment extends Fragment{

    @BindView(R.id.rvGuideContents) RecyclerView rvGuideContents;
    ArrayList<GuideContent> mguideContents;
    private TitleGuideContentAdapter mTitleGuideContentAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvGuideContents.addItemDecoration(new DividerItemDecoration(getContext(), 1));
        mguideContents = new ArrayList<GuideContent>();
        mTitleGuideContentAdapter = new TitleGuideContentAdapter(getContext(),
                mguideContents);
        rvGuideContents.setLayoutManager(new LinearLayoutManager(getContext()));
        rvGuideContents.setAdapter(mTitleGuideContentAdapter);

        mTitleGuideContentAdapter.setOnItemClickListener(new TitleGuideContentAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                String data = mguideContents.get(position).getId().toString();
//                Toast.makeText(getContext(), data, Toast.LENGTH_LONG).show();
                changeFragmentToSubGuideContent(data);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mguideContents.isEmpty()){
            getDataTitleGuideContent();
        }
    }

    public void getDataTitleGuideContent(){
        Call<ArrayList<GuideContent>> call = getApi().getTitleGuideContent();
        call.enqueue(new Callback<ArrayList<GuideContent>>() {
            @Override
            public void onResponse(Call<ArrayList<GuideContent>> call,
                                   Response<ArrayList<GuideContent>> response) {
                mTitleGuideContentAdapter.update(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<GuideContent>> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void changeFragmentToSubGuideContent(String id){
        Bundle arguments = new Bundle();
        arguments.putString("id_sub_guide", id);
        SubGuideContentFragment newFragment = new SubGuideContentFragment();
        newFragment.setArguments(arguments);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack if needed
        transaction.replace(R.id.flContent, newFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }
}
