package soft.bezzo.taxguide.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import soft.bezzo.taxguide.R;
import soft.bezzo.taxguide.adapter.SubGuideContentAdapter;
import soft.bezzo.taxguide.model.SubGuideContent;

/**
 * Created by bezzo on 14/03/17.
 */

import static soft.bezzo.taxguide.api.Client.getApi;

public class SubGuideContentFragment extends Fragment {

    @BindView(R.id.rvGuideContents)
        RecyclerView subGuideContentRV;
    private ArrayList<SubGuideContent> mSubGuideContents;
    private ArrayList<SubGuideContent> checkSubGuideContents = new ArrayList<SubGuideContent>();
    private SubGuideContentAdapter mSubGuideContentAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        subGuideContentRV.addItemDecoration(new DividerItemDecoration(getContext(), 1));
        mSubGuideContents = new ArrayList<SubGuideContent>();
        mSubGuideContentAdapter = new SubGuideContentAdapter(getContext(), mSubGuideContents);
        subGuideContentRV.setLayoutManager(new LinearLayoutManager(getContext()));
        subGuideContentRV.setAdapter(mSubGuideContentAdapter);

        mSubGuideContentAdapter.setOnItemClickListener(new SubGuideContentAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                final String data = mSubGuideContents.get(position).getId().toString();
                Call<ArrayList<SubGuideContent>> call = getApi().getSubTitleGuideContent(data);
                call.enqueue(new Callback<ArrayList<SubGuideContent>>() {
                    @Override
                    public void onResponse(Call<ArrayList<SubGuideContent>> call, Response<ArrayList<SubGuideContent>> response) {
                        if (response.body().size() == 0){
                            changeFragmentToContent(data);
                        }
                        else{
                            changeFragmentToSubGuideContent(data);
                        }
                    }

                    @Override
                    public void onFailure(Call<ArrayList<SubGuideContent>> call, Throwable t) {
                            Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    public void getDataSubGuideContent(String id){
        Call<ArrayList<SubGuideContent>> call = getApi().getSubTitleGuideContent(id);
        call.enqueue(new Callback<ArrayList<SubGuideContent>>() {
            @Override
            public void onResponse(Call<ArrayList<SubGuideContent>> call,
                                   Response<ArrayList<SubGuideContent>> response) {
                mSubGuideContentAdapter.update(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<SubGuideContent>> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle arguments = getArguments();
        getDataSubGuideContent(arguments.getString("id_sub_guide"));
    }

    public void changeFragmentToSubGuideContent(String id){
        Bundle arguments = new Bundle();
        arguments.putString("id_sub_guide", id);
        SubGuideContentFragment newFragment = new SubGuideContentFragment();
        newFragment.setArguments(arguments);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack if needed
        transaction.replace(R.id.flContent, newFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }

    public void changeFragmentToContent(String id){
        Bundle arguments = new Bundle();
        arguments.putString("id_sub_guide", id);
        ContentGuideFragment newFragment = new ContentGuideFragment();
        newFragment.setArguments(arguments);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack if needed
        transaction.replace(R.id.flContent, newFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }
}
